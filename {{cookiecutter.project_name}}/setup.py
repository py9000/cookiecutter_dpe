import sys
from setuptools import setup, find_packages

setup(
    name="app",
    version="0.0.1",
    description="lxc cloud",
    url="https://github.com/terminal-labs/app",
    author="Terminal Labs",
    author_email="solutions@terminallabs.com",
    license="see LICENSE file",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=[
        "setuptools",
    ],
    entry_points="""
        [console_scripts]
        app=app.__main__:main
    """,
)
